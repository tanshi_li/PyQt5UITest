# PyQt5UITest

PyQt5UI test代码，一个用于测试学习PyQt5UI演示项目

# Running
> Inside your preferred terminal run the commands below depending on your system, remembering before installing Python 3.9> and PyQt5 "pip install PyQt5".
> ## **Windows**:
```console
python uitest.py
```
# Compiling
> ## **Windows**:
```console
pyinstaller uitest.py
或
pyinstaller -D uitest.py
```